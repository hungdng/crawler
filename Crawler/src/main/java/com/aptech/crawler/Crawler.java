package com.aptech.crawler;

import com.aptech.model.News;
import com.aptech.model.Product;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.util.ArrayList;

public class Crawler {

    private static ArrayList<Product> getListProductByMenu(String URI){

        Document doc;
        ArrayList<Product> lstProduct = new ArrayList<>();
        try{
            doc = Jsoup.connect(URI).get();
            Elements links = doc.select("ul#menu-greenyhouse-menu > li#menu-item-283 > ul.sub-menu > li");
            for (Element link : links) {
                String linkUriCategory = link.select("a").attr("href");
                String categoryName = link.select("a").text();

                System.out.println("Đang cào => " + linkUriCategory);

                lstProduct.addAll(getListProductByCategory(linkUriCategory, categoryName));

                System.out.println(linkUriCategory + " => OK");
            }
        }
        catch (Exception ex){
            System.out.println("Lỗi: " + ex.getMessage());
        }
        return lstProduct;
    }

    private static ArrayList<Product> getListProductByCategory(String uri, String category){
        ArrayList<Product> lst = new ArrayList<>();
        Document doc;

        try{
            doc = Jsoup.connect(uri).get();
            Elements links = doc.select("div.woocommerce > nav.woocommerce-pagination");
            if(links.size() == 1){
                int pageSize = links.select("a.page-numbers").size();
                for (int i = 1; i <= pageSize; i++){
                    String uriPage = uri + "/page/" + i;
                    lst.addAll(getListProduct(uriPage, category));
                }
            }
            if(links.size() == 0){
                lst.addAll(getListProduct(uri, category));
            }
        }
        catch (Exception ex){
            System.out.println("Lỗi: " + ex.getMessage());
        }
        return lst;
    }

    private static ArrayList<Product> getListProduct(String uri, String category){
        Document doc;
        String MATH_UNIT = "Đơn";
        String MATH_UNIT_1 = "đơn";
        String MATH_MADE_IN = "Xuất xứ";
        ArrayList<Product> lstProduct = new ArrayList<Product>();

        try {
            doc = Jsoup.connect(uri).get();
            //Get all link
            Elements links = doc.select("ul.products > li");
            for (Element link : links) {
                String uriImage = link.select("a.product-images img").attr("src");

                Elements elProductName = link.select(".product-details-container > h3.product-title > a");
                String uriProduct = elProductName.attr("href");
                String productName = elProductName.text();

                Elements listPItem = link.select("div.product-details-container > p");
                String unit = "";
                String madein = "";
                for (Element item : listPItem) {
                    String StrOfP = item.text().trim();
                    if ((StrOfP.indexOf(MATH_UNIT) != -1) || (StrOfP.indexOf(MATH_UNIT_1) != -1)) {
                        unit = StrOfP.substring(StrOfP.indexOf(':') + 1, StrOfP.length()).trim();
                    }

                    if ((StrOfP.indexOf(MATH_MADE_IN) != -1) || (StrOfP.indexOf(MATH_MADE_IN) != -1)) {
                        madein = StrOfP.substring(StrOfP.indexOf(':') + 1, StrOfP.length()).trim();
                    }
                }

                String priceStr = link.select("div.product-details-container > div.clearfix > span.price > span.amount").text();
                priceStr = (priceStr.replaceAll("[\\,\\₫, \\\u00A0]", "")).trim();
                int price = Integer.parseInt(priceStr);

                Document docDetail = Jsoup.connect(uriProduct).get();
                Elements descriptions = docDetail.select("div#tab-description");
                String description = descriptions.text();

                //Add product to list

                lstProduct.add(new Product(productName, uriImage, price, unit, description, madein,category));
            }

        } catch (Exception ex) {
            System.out.println("Lỗi: " + ex.getMessage());
        }

        return lstProduct;
    }

    public static  void CrawlerProduct(){
        System.out.println("Bắt đầu cào Data.....");
        String URI = "http://greenyhouse.com";
        ArrayList<Product> lstProduct= getListProductByMenu(URI);

        System.out.println("Danh sách sản phẩm");
        for (Product item: lstProduct) {
            System.out.println(item.toString());
        }

        System.out.println("Kết thúc cào Data.....");
    }

    public static void CrawlerNew(){
        //CrawlerProduct();
        System.out.println("Bắt đầu cào Data.....");
        String uri = "http://fruitvietnam.com/tin-tuc/";
        String homeUrl = "http://fruitvietnam.com";
        ArrayList<News> lstNew= new ArrayList<News>();

        Document doc, docForPage;

        try {
            doc = Jsoup.connect(uri).get();
            //select pageSize
            Elements pageSize = doc.select("div.pagination > ul > li");

            for(int i = 1; i < pageSize.size(); i++){

                String urlPage = "http://fruitvietnam.com/tin-tuc-trang-"+ i +"/";

                System.out.println("Đang cào: " + urlPage);

                docForPage = Jsoup.connect(urlPage).get();
                // select all item new
                Elements links = docForPage.select("ol#products-list > li");
                for (Element link : links) {

                    String uriNew = homeUrl + link.select("a.product-image").attr("href");
                    String title = link.select("a.product-image").attr("title");
                    String uriImageStr = link.select("a.product-image img").attr("src");
                    String uriImageThumbnail = homeUrl + "/" + uriImageStr.substring(uriImageStr.indexOf("upload"), uriImageStr.indexOf("&w="));

                    String description = link.select("div.product-shop > div.no-fix > div.desc").text();


                    Document docDetail = Jsoup.connect(uriNew).get();
                    Elements fdescriptions = docDetail.select("article#center_column > ul.post-list > li");
                    String fdescription = fdescriptions.html();

                    //Add product to list

                    lstNew.add(new News(title, description, fdescription, uriImageThumbnail));
                }

                System.out.println(urlPage + " => OK");
            }
        } catch (Exception ex) {
            System.out.println("Lỗi: " + ex.getMessage());
        }

        System.out.println("Kết thúc cào Data.....");

        System.out.println("Danh sách tin tức");
        for (News item: lstNew) {
            System.out.println(item.toString());
        }
    }
    public static void main(String[] args) {
        CrawlerNew();
        CrawlerProduct();

    }
}
