package com.aptech.model;

public class Product {
    private String ProductName;
    private String UrlImage;
    private int Price;
    private String Unit;
    private String Description;
    private String Madein;
    private String Category;

    public Product() {
    }

    public Product(String productName, String urlImage, int price, String unit, String description, String madein, String category) {
        ProductName = productName;
        UrlImage = urlImage;
        Price = price;
        Unit = unit;
        Description = description;
        Madein = madein;
        Category = category;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getUrlImage() {
        return UrlImage;
    }

    public void setUrlImage(String urlImage) {
        UrlImage = urlImage;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }


    public String getMadein() {
        return Madein;
    }

    public void setMadein(String madein) {
        Madein = madein;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    @Override
    public String toString() {
        return "Product{" +
                "ProductName='" + ProductName + '\'' +
                ", UrlImage='" + UrlImage + '\'' +
                ", Price=" + Price +
                ", Unit='" + Unit + '\'' +
                ", Description='" + Description + '\'' +
                ", Madein='" + Madein + '\'' +
                ", Category='" + Category + '\'' +
                '}';
    }
}
