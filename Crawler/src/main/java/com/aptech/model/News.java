package com.aptech.model;

public class News {

    private String title;
    private String description;
    private String fullDescription;
    private String thumbnail;

    public News() {
    }

    public News(String title, String description, String fullDescription, String thumbnail) {
        this.title = title;
        this.description = description;
        this.fullDescription = fullDescription;
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", fullDescription='" + fullDescription + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
}
